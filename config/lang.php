<?php

    return [

        /*
         * Enable / Disable Multi-Language Interface
         * =========================
         *
         * Settings flag to enable / disable multilingual site. Default: false
         * The default language is set to app.config.locale
         *
         */
        'enable' => env('LANG_ENABLE',false),

        /*
         * Localization languages list
         * ==========================
         *
         * Localization language has alias as key
         *
         */

        'list' => env('LANG_LIST', [
            'ru'    => 'Русский язык',
            'en'    => 'English',
        ]),

        /*
         * Interface language mappings in page routes
         * ===========================
         *
         * Always show language alias in link
         * Default: false - default language is available without alias in url
         *
         */
        'always_display' => env('LANG_ALWAYS_DISPLAY', false),
    ];
