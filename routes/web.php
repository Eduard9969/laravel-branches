<?php

use App\Http\Middleware\Locale as LocaleMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$lang_config  = LocaleMiddleware::getLangParams();
$lang_enable  = $lang_config['enable']   ?? false;
$display_lang = $lang_config['display']  ?? false;
$locale       = $lang_config['lang']     ?? null;
$app_locale   = $lang_config['app_lang'] ?? null;

/*
 * Anonymous Routes
 * =================
 *
 * The main routes for building a redirect with a constant display of the language alias in url
 *
 */
$anonymous = function() {
    Route::get('/', function() {
        return redirect('/' . LocaleMiddleware::getLang(true) . '/', 301);
    });

    Route::any('/{any}', function($any) {
        return redirect('/' . LocaleMiddleware::getLang(true) . '/' . $any, 301);
    });
};

/*
 * Master Routes
 * =================
 *
 * Default route map for processing application requests
 * All routes are built in this container. The basis for multi language
 *
 */
$master = function() {
    Route::get('/', 'Main\MainController@index')->name('main');
};

/*
 * Request Routing with Language Initialization
 * If prefix NULL and not always display lang in link, the main language is used, without alias
 */
$anonymous_trigger = ($lang_enable && $display_lang && empty($locale) && !empty($app_locale));
Route::group(['prefix' => $locale], ($anonymous_trigger ? $anonymous : $master));
