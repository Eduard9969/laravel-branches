const mix        = require('laravel-mix'),
      autoprefix = require('autoprefixer');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/dist/js');

/*
 * Sass Params
 */
let params = {
    autoprefixer: {
        options: {
            browsers: [
                'last 6 versions',
            ]
        }
    }
};

mix.sass('resources/sass/app.scss', 'public/dist/css').options(params)
    .sass('resources/sass/critical.scss', 'public/dist/css').options(params);
