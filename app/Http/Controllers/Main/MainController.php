<?php


namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

/**
 * Class MainController
 * @package App\Http\Controllers\Main
 */
class MainController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

}
