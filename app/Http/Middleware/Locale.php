<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;

/**
 * Class Locale
 * Custom Middleware Class to initialize dynamic languages
 *
 * @package App\Http\Middleware
 */
class Locale
{

    /**
     * @var bool $enable
     */
    private static $enable = false;

    /**
     * @var string main_lang
     */
    private static $main_lang = '';

    /**
     * @var array langs
     */
    private static $langs = [];

    /**
     * @var bool $always_display_alias
     */
    private static $display_alias = false;

    /**
     * Locale constructor.
     */
    public function __construct()
    {
        self::init();
    }

    /**
     * Init global language variables
     */
    private static function init()
    {
        $config = config('lang');
        self::$enable = (bool) $config['enable'] ?? false;

        if(self::$enable)
        {
            self::$langs         = (array)  isset($config['list']) ? array_keys($config['list']) : [];
            self::$main_lang     = (string) isset(self::$langs[0]) ? self::$langs[0] : null;

            self::$display_alias = (bool) $config['always_display'] ?? false;
        }
    }

    /**
     * Get a dynamic language alias
     *
     * @return mixed|null
     */
    private static function getLocale()
    {
        $uri         = Request::path();
        $segmentsUri = explode('/', $uri);

        if(self::$enable && isset($segmentsUri[0]) && in_array($segmentsUri[0], self::$langs))
            return $segmentsUri[0];

        return null;
    }

    /**
     * Get Lang Param Array
     *
     * @return array
     */
    public static function getLangParams()
    {
        self::init();

        return [
            'enable'    => self::$enable,
            'display'   => self::$display_alias,
            'lang'      => self::getLocale(),
            'app_lang'  => App::getLocale(),
            'list'      => self::$langs,
        ];
    }

    /**
     * Get Lang Variable
     *
     * @param bool $main_lang
     * @return mixed|null
     */
    public static function getLang($main_lang = false)
    {
        self::init();
        return $main_lang ? self::getMainLocale() : self::getLocale();
    }

    /**
     * Get a main language alias
     *
     * @return mixed|string|null
     */
    private static function getMainLocale()
    {
        return !empty(self::$main_lang) ? self::$main_lang : App::getLocale();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(self::$enable)
        {
            $locale = self::getLocale();

            if($locale) App::setLocale($locale);
            else App::setLocale(self::getMainLocale());
        }

        return $next($request);
    }
}
